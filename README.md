# Swagger Library

Swagger Library is library to generate Open API Spec and also provide Swagger UI.

## Setup Dependency

```xml
<dependency>
    <groupId>com.gitlab.residwi.library</groupId>
    <artifactId>swagger</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>

<!-- add repository -->
<repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/groups/11285654/-/packages/maven</url>
    </repository>
</repositories>
```

## Open API and Swagger UI

This library will automatically generate open api spec and swagger ui. This library will read all Controller class and automatically generate open api spec.

```
// Open API Spec
GET /v3/api-docs

// Swagger UI
GET /swagger-ui.html
``` 

## Ignored Parameter for Swagger

By default, swagger will transform all parameter on a controller to become body or query param.
If we need to ignore it, we can use annotation `@SwaggerIgnore`.

```java
@RestController
public class CustomerController {
  
  @GetMapping("/customers/{customerId}")
  public ResponsePayload<Customer> get(@SwaggerIgnored MandatoryParameter mandatoryParameter,
                                      @PathVariable("customerId") String customerId){
    ...
  } 

}
```

## Swagger Properties

We can configure swagger information on Open API Spec

```properties
library.swagger.title=Hello World API
library.swagger.version=1.0.0
library.swagger.description=Example Description
library.swagger.terms-of-service=https://www.example.com/
```