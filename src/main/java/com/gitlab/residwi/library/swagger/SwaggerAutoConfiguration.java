package com.gitlab.residwi.library.swagger;

import com.gitlab.residwi.library.swagger.annotation.SwaggerIgnored;
import com.gitlab.residwi.library.swagger.factory.ComponentsFactoryBean;
import com.gitlab.residwi.library.swagger.factory.OpenAPIFactoryBean;
import com.gitlab.residwi.library.swagger.properties.SwaggerProperties;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.parameters.Parameter;
import org.springdoc.core.SpringDocAnnotationsUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({
        SwaggerProperties.class
})
public class SwaggerAutoConfiguration {

    public SwaggerAutoConfiguration() {
        SpringDocAnnotationsUtils.addAnnotationsToIgnore(SwaggerIgnored.class);
    }

    @Bean
    public ComponentsFactoryBean components(ApplicationContext applicationContext) {
        var parameters = applicationContext.getBeansOfType(Parameter.class);

        var componentsFactoryBean = new ComponentsFactoryBean();
        componentsFactoryBean.setParameters(parameters);
        return componentsFactoryBean;
    }

    @Bean
    public OpenAPIFactoryBean openAPI(@Autowired Components components,
                                      @Autowired SwaggerProperties swaggerProperties) {
        var openAPIFactoryBean = new OpenAPIFactoryBean();
        openAPIFactoryBean.setComponents(components);
        openAPIFactoryBean.setSwaggerProperties(swaggerProperties);
        return openAPIFactoryBean;
    }
}
